package com.arquitectura;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceException;
import javax.persistence.Table;
import javax.persistence.TypedQuery;




@Entity
@Table(name="libros")
public class Libro {
	@Id
	private String isbn;
	@Column(name="titulo")
	private String titulo;
	
	@ManyToOne
	@JoinColumn(name="categoria")
	private Categoria categoria;
	
	public Libro() {
		super();
	}
	public Libro(String isbn, String titulo, Categoria categoria)
	{
		super();
		this.categoria = categoria;
		this.titulo = titulo;
		this.isbn = isbn;
	}
	public Libro(String isbn)
	{
		super();
		this.isbn=isbn;
		this.titulo="";
		this.categoria=null;
			
	}
	@Override
	public int hashCode() {
		return isbn.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		String isbnLibro = ((Libro)o).getIsbn();
		return isbnLibro.equals(this.isbn);
	}
	
	@SuppressWarnings("unchecked")
	public static List<String> buscarTodasLasCategorias() 
	{
		List<String> listaDeCategorias = null;
		
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		TypedQuery consulta = manager.createQuery(
				"SELECT DISTINCT I.categoria.descripcion FROM Libro I ", String.class);
		try {
		listaDeCategorias = consulta.getResultList();
		}finally{
			manager.close();
		}
		return listaDeCategorias;
	}
	
	public  void insertar() {
		EntityManagerFactory factoriaSession = JPAHelper.getJPAFactory(); 
		EntityManager manager = factoriaSession.createEntityManager();
		EntityTransaction tx = null;
		
		try {
		tx = manager.getTransaction();
		tx.begin();
		manager.persist(this);
		tx.commit();
		}catch(PersistenceException e) {
			tx.rollback();
		}finally {
			manager.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static  List<Libro> buscarTodos() 
	{
		EntityManagerFactory  factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		TypedQuery<Libro> consulta = manager.
				createQuery("SELECT I FROM Libro I JOIN FETCH I.categoria",
						Libro.class);
		List<Libro> listaDeLibros = null;
		
		try {
			listaDeLibros = consulta.getResultList();
		
		}finally{
			manager.close();
		}
		return listaDeLibros;
	}
	
	public void borrar() 
	{
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		EntityTransaction tx = null;
		
		try { 
		tx = manager.getTransaction();
		tx.begin();
		manager.remove(manager.merge(this));
		tx.commit();
		}catch(PersistenceException e){
			manager.getTransaction().rollback();
		}finally {
		manager.close();
		}
	}
	
	public void salvar()  {
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		EntityTransaction tx = null;
		
		try {
			tx = manager.getTransaction();
			tx.begin();
			manager.merge(this);
			tx.commit();
		}finally {
			manager.close();
		}
	}
	
	public static Libro buscarPorClave(String isbn) 
	{
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		TypedQuery<Libro> consulta = manager.createQuery(
				"Select I from Libro I JOIN FETCH I.categoria where I.isbn=?1",
				Libro.class);
		Libro libro = null;
		try{
			consulta.setParameter(1, isbn);
			
			libro = consulta.getSingleResult();
		}finally {
			manager.close();
		}
		return libro;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Libro> buscarPorCategoria(Categoria categoria) 
	{
		List<Libro>	listaDeLibros = null;
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		
		TypedQuery<Libro> consulta = manager.createQuery(
				"Select I from Libro I JOIN FETCH I.categoria where I.categoria=?1",
				Libro.class);
		try {
		consulta.setParameter(1,categoria);
		listaDeLibros = consulta.getResultList();
		}finally {
			manager.close();
		}
		return listaDeLibros;
	}
	
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
} 