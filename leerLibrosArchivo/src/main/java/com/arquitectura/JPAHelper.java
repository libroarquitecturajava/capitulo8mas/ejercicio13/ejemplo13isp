package com.arquitectura;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class JPAHelper {
	
	private static final EntityManagerFactory emf = builtEntityManagerFactory();
	
	private static EntityManagerFactory builtEntityManagerFactory(){
		try {
			return Persistence.createEntityManagerFactory("arquitecturajava");
		}catch(Throwable ex) {
			throw new RuntimeException("Error al crearla factoria de JPA");
		}
	} 

public static EntityManagerFactory getJPAFactory() {
		return emf;
	}
}