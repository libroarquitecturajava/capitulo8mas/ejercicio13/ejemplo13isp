package com.arquitectura;


import java.util.List;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceException;

@Entity
@Table(name="categorias")
public class Categoria {
	@Id
	@Column(name="id_categoria")
	private int id;
	
	@Column(name="descripcion")
	private String descripcion;
	
	@OneToMany
	@JoinColumn(name="categoria")
	private List<Libro> listaDelibro;
	
	public int hashCode() {
		return id;
	}
	
	
	public Categoria() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Categoria(String descripcion) {
		super();
		this.descripcion = descripcion;
	}
	
	public Categoria(int id,String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}


	@Override
	public boolean equals(Object o) {
		int categoriaId = ((Categoria)o).getId();
		return (id == categoriaId);
	}
	
	public static List<Categoria> buscarTodos(){
		List<Categoria>  lista = null;
		String cadConsulta = "FROM Categoria categorias";
		
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		try {
		TypedQuery consulta = manager.createQuery(cadConsulta, Categoria.class);
		lista = consulta.getResultList();
		}finally {
			manager.close();
		}
		return lista;
	}
	
	public  void insertar() {
		EntityManagerFactory factoriaSession = JPAHelper.getJPAFactory(); 
		EntityManager manager = factoriaSession.createEntityManager();
		EntityTransaction tx = null;
		
		try {
		tx = manager.getTransaction();
		tx.begin();
		manager.persist(this);
		tx.commit();
		}catch(PersistenceException e) {
			tx.rollback();
		}finally {
			manager.close();
		}
	}
	
	public static Categoria buscaCategoria(int id) {
		Categoria categoria = null;
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		TypedQuery<Categoria> consulta = manager.createQuery(
				"SELECT C FROM Categoria C WHERE C.id=?1",Categoria.class);
		try {
			consulta.setParameter(1, id);
			categoria = consulta.getSingleResult();	
		}finally {
			manager.close();
		}
		
	 return categoria ;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Libro> getListaDelibro() {
		return listaDelibro;
	}

	public void setListaDelibro(List<Libro> listaDelibro) {
		this.listaDelibro = listaDelibro;
	}
	
	
	
}
