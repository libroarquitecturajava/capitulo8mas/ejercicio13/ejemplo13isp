package com.arquitectura;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class Principal {

	public static void main(String[] args) throws IOException, FileNotFoundException
	{
		String texto ="";
		Reader I;
		Principal p = new Principal();
		
			I = new FileReader( p.obtenerRutaDentroProyecto());
		
		BufferedReader lector = new BufferedReader(I);
		List<Libro> lista = new ArrayList<Libro>();
		Libro libro=null;
		Categoria categoria = null;
		
		while((texto= lector.readLine())!=null) {
			String[] datos = texto.split(",");
			categoria  = new Categoria(Integer.parseInt(datos[2]),datos[3]);
			categoria.insertar();
			libro = new Libro(datos[0],datos[1],categoria);
			libro.insertar();
			lista.add(libro);
		}
		
		for(Libro lib : lista) {
			System.out.println(lib.getTitulo());
		}
	}
	
	public String obtenerRutaDentroProyecto() {
		return getClass().
				getClassLoader().getResource("libros.txt").getFile();
	}
}
